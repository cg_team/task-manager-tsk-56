
package ru.inshakov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "DataEndpoint", targetNamespace = "http://endpoint.tm.inshakov.ru/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface DataEndpoint {


    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "saveDataBase64", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataBase64")
    @ResponseWrapper(localName = "saveDataBase64Response", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataBase64Response")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataBase64Request", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataBase64Response")
    public void saveDataBase64(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "loadDataXml", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataXml")
    @ResponseWrapper(localName = "loadDataXmlResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataXmlResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataXmlRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataXmlResponse")
    public void loadDataXml(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "saveDataJsonJaxB", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataJsonJaxB")
    @ResponseWrapper(localName = "saveDataJsonJaxBResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataJsonJaxBResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataJsonJaxBRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataJsonJaxBResponse")
    public void saveDataJsonJaxB(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "loadDataBin", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataBin")
    @ResponseWrapper(localName = "loadDataBinResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataBinResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataBinRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataBinResponse")
    public void loadDataBin(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "saveDataBin", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataBin")
    @ResponseWrapper(localName = "saveDataBinResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataBinResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataBinRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataBinResponse")
    public void saveDataBin(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "loadDataJsonJaxB", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataJsonJaxB")
    @ResponseWrapper(localName = "loadDataJsonJaxBResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataJsonJaxBResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataJsonJaxBRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataJsonJaxBResponse")
    public void loadDataJsonJaxB(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "loadDataBase64", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataBase64")
    @ResponseWrapper(localName = "loadDataBase64Response", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataBase64Response")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataBase64Request", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataBase64Response")
    public void loadDataBase64(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "saveDataJson", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataJson")
    @ResponseWrapper(localName = "saveDataJsonResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataJsonResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataJsonRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataJsonResponse")
    public void saveDataJson(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "saveDataXmlJaxB", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataXmlJaxB")
    @ResponseWrapper(localName = "saveDataXmlJaxBResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataXmlJaxBResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataXmlJaxBRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataXmlJaxBResponse")
    public void saveDataXmlJaxB(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "loadDataXmlJaxB", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataXmlJaxB")
    @ResponseWrapper(localName = "loadDataXmlJaxBResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataXmlJaxBResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataXmlJaxBRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataXmlJaxBResponse")
    public void loadDataXmlJaxB(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "saveDataXml", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataXml")
    @ResponseWrapper(localName = "saveDataXmlResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataXmlResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataXmlRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataXmlResponse")
    public void saveDataXml(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "saveDataYaml", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataYaml")
    @ResponseWrapper(localName = "saveDataYamlResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.SaveDataYamlResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataYamlRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/saveDataYamlResponse")
    public void saveDataYaml(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "loadDataJson", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataJson")
    @ResponseWrapper(localName = "loadDataJsonResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataJsonResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataJsonRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataJsonResponse")
    public void loadDataJson(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

    /**
     * 
     * @param session
     */
    @WebMethod
    @RequestWrapper(localName = "loadDataYaml", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataYaml")
    @ResponseWrapper(localName = "loadDataYamlResponse", targetNamespace = "http://endpoint.tm.inshakov.ru/", className = "ru.inshakov.tm.endpoint.LoadDataYamlResponse")
    @Action(input = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataYamlRequest", output = "http://endpoint.tm.inshakov.ru/DataEndpoint/loadDataYamlResponse")
    public void loadDataYaml(
        @WebParam(name = "session", targetNamespace = "")
        Session session);

}
