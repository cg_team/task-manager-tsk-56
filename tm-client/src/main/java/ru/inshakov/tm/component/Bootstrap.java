package ru.inshakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.exception.system.UnknownCommandException;
import ru.inshakov.tm.service.CommandService;
import ru.inshakov.tm.service.LoggerService;
import ru.inshakov.tm.util.SystemUtil;
import ru.inshakov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

@Getter
@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    protected FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractCommand[] commands;

    @NotNull
    @Autowired
    private CommandService commandService;

    @NotNull
    @Autowired
    private LoggerService loggerService;

    public void init() {
        initPID();
        initCommands();
    }

    public void run(@Nullable final String... args) {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        init();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseCommand(@Nullable final String name) {
        if (!Optional.ofNullable(name).isPresent()) return;
        final AbstractCommand abstractCommand = commandService.getCommandByName(name);
        Optional.ofNullable(abstractCommand).orElseThrow(() -> new UnknownCommandException(name));
        abstractCommand.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    @SneakyThrows
    private void initCommands(){
        if (commands == null) return;
        Arrays.stream(commands).filter(Objects::nonNull).forEach(t -> commandService.add(t));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
