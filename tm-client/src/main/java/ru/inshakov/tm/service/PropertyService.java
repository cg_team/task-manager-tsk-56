package ru.inshakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

import static ru.inshakov.tm.constant.PropertyConst.*;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY))
            return System.getenv(APPLICATION_VERSION_KEY);
        if (System.getProperties().containsKey(APPLICATION_VERSION_KEY))
            return System.getProperty(APPLICATION_VERSION_KEY);
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_VALUE);
    }

}
