package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class TaskShowByIndexCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-show-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Task task = taskEndpoint.findTaskByIndex(getSession(), index);
        if (task == null) throw new TaskNotFoundException();
        show(task);
    }

}
