package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProjectShowByNameCommand extends ProjectAbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = projectEndpoint.findProjectByName(getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }

}
