package ru.inshakov.tm.command.system;

import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AbstractCommand;

@Component
public class ExitCommand extends AbstractCommand {
    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
