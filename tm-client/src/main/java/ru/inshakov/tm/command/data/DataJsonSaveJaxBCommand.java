package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AuthAbstractCommand;
import ru.inshakov.tm.endpoint.DataEndpoint;

@Component
public class DataJsonSaveJaxBCommand extends AuthAbstractCommand {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-save-json-j";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Save data to JSON by JaxB.";
    }

    public void execute() {
        dataEndpoint.saveDataJsonJaxB(getSession());
    }

}