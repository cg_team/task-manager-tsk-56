package ru.inshakov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AuthAbstractCommand;
import ru.inshakov.tm.endpoint.SessionEndpoint;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProfileUpdateCommand extends AuthAbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update profile.";
    }

    @Override
    public void execute() {
        System.out.println("Enter first name");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        @Nullable final String middleName = TerminalUtil.nextLine();
        sessionEndpoint.updateUser(getSession(), firstName, lastName, middleName);
    }

}
