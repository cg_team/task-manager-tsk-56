package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIndexCommand extends ProjectAbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = projectEndpoint.findProjectByIndex(getSession(), index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Project projectUpdated = projectEndpoint
                .updateProjectByIndex(getSession(), index, name, description);
        if (projectUpdated == null) System.out.println("Failed");
    }

}
