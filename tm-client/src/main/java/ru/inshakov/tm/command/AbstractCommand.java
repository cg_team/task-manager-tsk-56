package ru.inshakov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.service.SessionService;

public abstract class AbstractCommand {

    @Nullable
    @Autowired
    protected SessionService sessionLocator;

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String arg = arg();
        @Nullable final String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

    @Nullable
    protected Session getSession() {
        @Nullable Session session = sessionLocator.getSession();
        if (session == null) throw new AccessDeniedException();
        return session;
    }

}
