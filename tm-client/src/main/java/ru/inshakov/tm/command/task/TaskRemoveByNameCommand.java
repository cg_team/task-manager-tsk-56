package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class TaskRemoveByNameCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        taskEndpoint.removeTaskByName(getSession(), name);
    }
}
