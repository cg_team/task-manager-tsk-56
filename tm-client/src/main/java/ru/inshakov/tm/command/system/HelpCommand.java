package ru.inshakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.service.CommandService;

@Component
public class HelpCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private CommandService commandService;

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : commandService.getCommands()) {
            System.out.println(command.toString());
        }
    }
}
