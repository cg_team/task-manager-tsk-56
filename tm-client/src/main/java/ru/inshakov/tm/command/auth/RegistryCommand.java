package ru.inshakov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AuthAbstractCommand;
import ru.inshakov.tm.endpoint.SessionEndpoint;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class RegistryCommand extends AuthAbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        @Nullable final String email = TerminalUtil.nextLine();
        sessionEndpoint.register(login, password, email);
    }

}
