package ru.inshakov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AuthAbstractCommand;
import ru.inshakov.tm.endpoint.SessionEndpoint;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class PasswordChangeCommand extends AuthAbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "password-change";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change password.";
    }

    @Override
    public void execute() {
        System.out.println("Enter new password");
        @Nullable final String password = TerminalUtil.nextLine();
        sessionEndpoint.setPassword(getSession(), password);
    }

}
