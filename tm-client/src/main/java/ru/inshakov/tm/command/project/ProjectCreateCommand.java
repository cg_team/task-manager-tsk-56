package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.endpoint.ProjectEndpoint;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class ProjectCreateCommand extends ProjectAbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Project project = add(name, description);
        projectEndpoint.addProject(getSession(), project);
    }

}
