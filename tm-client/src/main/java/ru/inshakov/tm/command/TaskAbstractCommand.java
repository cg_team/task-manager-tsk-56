package ru.inshakov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;


public abstract class TaskAbstractCommand extends AbstractCommand {

    protected void show(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + Status.valueOf(task.getStatus().value()).getDisplayName());
        System.out.println("ProjectDto Id: " + task.getProjectId());
    }

    @NotNull
    protected Task add(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    protected String toString(Task task) {
        return task.getId() + ": " + task.getName();
    }

}
