package ru.inshakov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AuthAbstractCommand;
import ru.inshakov.tm.endpoint.Session;
import ru.inshakov.tm.endpoint.SessionEndpoint;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class LoginCommand extends AuthAbstractCommand {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logging in to application.";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        Session session = sessionEndpoint.open(login, password);
        sessionLocator.setSession(session);
    }

}
