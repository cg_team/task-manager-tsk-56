package ru.inshakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AbstractCommand;

@Component
public class AboutCommand extends AbstractCommand {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }
}
