package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Task task = taskEndpoint.findTaskById(getSession(), id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = taskEndpoint.updateTaskById(getSession(), id, name, description);
        if (taskUpdated == null) System.out.println("Failed");
    }

}
