package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.ProjectEndpoint;

@Component
public class ProjectClearCommand extends ProjectAbstractCommand {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    public void execute() {
        projectEndpoint.clearProject(getSession());
    }

}
