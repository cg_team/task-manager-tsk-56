package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.endpoint.TaskEndpoint;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

@Component
public class TaskBindTaskToProjectByIdCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-bind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = taskEndpoint.findTaskById(getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        taskEndpoint.bindTaskById(getSession(), taskId, projectId);
    }
}
