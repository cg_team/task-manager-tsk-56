package ru.inshakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.service.PropertyService;

@Component
public class VersionCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println(propertyService.getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }
}
