package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.TaskEndpoint;

@Component
public class TaskClearCommand extends TaskAbstractCommand {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Override
    public void execute() {
        taskEndpoint.clearTask(getSession());
    }

}
