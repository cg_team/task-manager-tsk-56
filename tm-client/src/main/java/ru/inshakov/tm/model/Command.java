package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public class Command {

    @Nullable
    private String name;

    @Nullable
    private String arg;

    @Nullable
    private String description;

    @NotNull
    public Command(@Nullable String name, @Nullable String arg, @Nullable String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "] ";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
