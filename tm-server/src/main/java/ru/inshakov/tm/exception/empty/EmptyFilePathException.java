package ru.inshakov.tm.exception.empty;

public final class EmptyFilePathException extends RuntimeException {

    public EmptyFilePathException() {
        super("Error! File path is empty...");
    }

}
