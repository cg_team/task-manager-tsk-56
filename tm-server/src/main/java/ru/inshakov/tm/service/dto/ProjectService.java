package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.repository.dto.IProjectRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.dto.IProjectService;
import ru.inshakov.tm.dto.Project;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.repository.dto.ProjectRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<Project> collection) {
        if (collection == null) return;
        for (Project item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    public Project add(@Nullable final Project entity) {
        if (entity == null) return null;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.add(entity);
            repository.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.clear();
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Project entity) {
        if (entity == null) return;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeById(entity.getId());
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }


    @NotNull
    @Override
    @SneakyThrows
    public Project findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            if (index > repository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
            return repository.findByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            return repository.findByName(userId, name);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            if (index > repository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
            repository.removeByIndex(userId, index);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByName(userId, name);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex
            (@NotNull final String userId, @Nullable final Integer index,
             @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final Project project = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    public Project add(String userId, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            return repository.findAllByUserId(userId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<Project> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (Project item : collection) {
            item.setUserId(userId);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(final String userId, @Nullable final Project entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @Nullable final Project entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.clearByUserId(userId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final Project entity) {
        if (entity == null) return;
        @NotNull final IProjectRepository repository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByIdUserId(userId, entity.getId());
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
