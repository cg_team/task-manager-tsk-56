package ru.inshakov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.repository.dto.IProjectRepository;
import ru.inshakov.tm.api.repository.dto.ITaskRepository;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.dto.Task;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.repository.dto.ProjectRepository;
import ru.inshakov.tm.repository.dto.TaskRepository;
import ru.inshakov.tm.service.AbstractEntityService;

import java.util.List;
import java.util.Optional;

@Service
public final class ProjectTaskService extends AbstractEntityService implements IProjectTaskService {

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        try {
            return repository.findAllTaskByProjectId(userId, projectId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        try {
            repository.getTransaction().begin();

            repository.bindTaskToProjectById(userId, taskId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        try {
            repository.getTransaction().begin();

            repository.unbindTaskById(userId, taskId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository repository = context.getBean(TaskRepository.class);
        @NotNull final IProjectRepository projectRepository = context.getBean(ProjectRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}