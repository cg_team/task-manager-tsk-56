package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.repository.dto.IProjectRepository;
import ru.inshakov.tm.api.repository.model.IProjectGraphRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.model.IProjectGraphService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.model.ProjectGraph;
import ru.inshakov.tm.model.UserGraph;
import ru.inshakov.tm.repository.model.ProjectGraphRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public final class ProjectGraphService extends AbstractGraphService<ProjectGraph> implements IProjectGraphService {

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll() {
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {

            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<ProjectGraph> collection) {
        if (collection == null) return;
        for (ProjectGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    public ProjectGraph add(@Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();

            repository.add(entity);
            repository.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {

            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final List<ProjectGraph> projects = repository.findAll();
            for (ProjectGraph t :
                    projects) {
                repository.remove(t);
            }
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();

            @Nullable final ProjectGraph project = repository.getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (project == null) return;
            repository.remove(project);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();

            @Nullable final ProjectGraph project = repository.getReference(entity.getId());
            if (project == null) return;
            repository.remove(project);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }


    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            if (index > repository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
            return repository.findByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            return repository.findByName(userId, name);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            if (index > repository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
            @Nullable final ProjectGraph project = repository.findByIndex(userId, index);
            if (project == null) return;
            repository.remove(project);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final ProjectGraph project = repository.findByName(userId, name);
            if (project == null) return;
            repository.remove(project);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph updateByIndex
            (@NotNull final String userId, @Nullable final Integer index,
             @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.IN_PROGRESS);
            project.setStartDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final ProjectGraph project = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(Status.COMPLETED);
            project.setFinishDate(new Date());
            repository.update(project);
            repository.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    public ProjectGraph add(@NotNull UserGraph user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph project = new ProjectGraph(name, description);
        project.setUser(user);
        return add(project);
    }

    @NotNull
    @Override
    public List<ProjectGraph> findAll(@NotNull final String userId) {
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {

            return repository.findAllByUserId(userId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph user, @Nullable final Collection<ProjectGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (ProjectGraph item : collection) {
            item.setUser(user);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph add(@NotNull final UserGraph user, @Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        entity.setUser(user);
        @Nullable final ProjectGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {

            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final List<ProjectGraph> projects = repository.findAllByUserId(userId);
            for (ProjectGraph t :
                    projects) {
                repository.remove(t);
            }
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();

            @Nullable final ProjectGraph project = repository.findByIdUserId(
                    userId,
                    optionalId.orElseThrow(EmptyIdException::new)
            );
            if (project == null) return;
            repository.remove(project);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @NotNull final IProjectGraphRepository repository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();

            @Nullable final ProjectGraph project = repository.findByIdUserId(userId, entity.getId());
            if (project == null) return;
            repository.remove(project);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
