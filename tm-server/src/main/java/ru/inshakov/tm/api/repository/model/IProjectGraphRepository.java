package ru.inshakov.tm.api.repository.model;

import ru.inshakov.tm.api.IGraphRepository;
import ru.inshakov.tm.model.ProjectGraph;

import java.util.List;

public interface IProjectGraphRepository extends IGraphRepository<ProjectGraph> {

    void update(final ProjectGraph projectGraph);

    ProjectGraph findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<ProjectGraph> findAllByUserId(final String userId);

    ProjectGraph findByName(final String userId, final String name);

    ProjectGraph findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

}
