package ru.inshakov.tm.api;

import ru.inshakov.tm.model.AbstractGraphEntity;

import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

public interface IGraphRepository<E extends AbstractGraphEntity> {

    List<E> findAll();

    void add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

    E getReference(final String id);

    void close();

    EntityTransaction getTransaction();

}