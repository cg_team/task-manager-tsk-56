package ru.inshakov.tm.api;

import ru.inshakov.tm.dto.AbstractEntity;

import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    void add(final E entity);

    void addAll(final Collection<E> collection);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void close();

    EntityTransaction getTransaction();

}