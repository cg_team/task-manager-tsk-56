package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class AbstractEntityRepository {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    public void close() {
        entityManager.close();
    }

    public EntityTransaction getTransaction() {
        return entityManager.getTransaction();
    }
}
