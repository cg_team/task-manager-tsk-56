package ru.inshakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.inshakov.tm.api.repository.dto.ISessionRepository;
import ru.inshakov.tm.dto.Session;

import java.util.List;

@Repository
@Scope("prototype")
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public List<Session> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM Session e WHERE e.userId = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM Session e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    public Session findById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM Session e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        Session reference = entityManager.getReference(Session.class, id);
        entityManager.remove(reference);
    }
}