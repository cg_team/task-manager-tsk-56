package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.inshakov.tm.api.service.dto.IProjectService;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.dto.Project;
import ru.inshakov.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @WebMethod
    public List<Project> findProjectAll(@WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        return projectService.findAll(session.getUserId());
    }

    @WebMethod
    public void addProjectAll(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") final Collection<Project> collection
    ) {
        sessionService.validate(session);
        projectService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public Project addProject(
            @WebParam(name = "session") final Session session, @WebParam(name = "entity") final Project entity
    ) {
        sessionService.validate(session);
        return projectService.add(session.getUserId(), entity);
    }

    @WebMethod
    public Project addProjectWithName(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        return projectService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearProject(@WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session") final Session session, @WebParam(name = "entity") final Project entity
    ) {
        sessionService.validate(session);
        projectService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public Project findProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        return projectService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        return projectService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public Project startProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @WebMethod
    public Project startProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project startProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public Project finishProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public Project finishProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Project finishProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session") final Session session, @WebParam(name = "projectId") final String projectId
    ) {
        sessionService.validate(session);
        projectService.removeById(session.getUserId(), projectId);
    }

    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        projectService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        projectService.removeByName(session.getUserId(), name);
    }
}
