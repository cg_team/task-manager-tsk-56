package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.inshakov.tm.api.service.dto.ISessionService;
import ru.inshakov.tm.api.service.dto.IUserService;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @WebMethod
    public Session open(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {

        Session session = sessionService.open(login, password);
        return session;
    }

    @WebMethod
    public void close(@WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        sessionService.close(session);
    }

    @WebMethod
    public Session register(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        userService.add(login, password, email);
        return sessionService.open(login, password);
    }

    @WebMethod
    public User setPassword(
            @WebParam(name = "session") final Session session, @WebParam(name = "password") final String password
    ) {
        sessionService.validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public User updateUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        sessionService.validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
