package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.inshakov.tm.api.service.dto.IUserService;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.User;
import ru.inshakov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
public final class AdminEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session") final Session session, @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @WebMethod
    public User lockByLogin(
            @WebParam(name = "session") final Session session, @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.lockByLogin(login);
    }

    @WebMethod
    public User unlockByLogin(
            @WebParam(name = "session") final Session session, @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.unlockByLogin(login);
    }

    @WebMethod
    public void closeAllByUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String userId
    ) {
        sessionService.validate(session, Role.ADMIN);
        sessionService.closeAllByUserId(userId);
    }

    @Nullable
    @WebMethod
    public List<Session> findAllByUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String userId
    ) {
        sessionService.validate(session, Role.ADMIN);
        return sessionService.findAllByUserId(userId);
    }
}
