package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.api.service.dto.ITaskService;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @WebMethod
    public List<Task> findTaskAll(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @WebMethod
    public void addTaskAll(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "collection") final Collection<Task> collection
    ) {
        sessionService.validate(session);
        taskService.addAll(session.getUserId(), collection);
    }

    @WebMethod
    public Task addTask(
            @WebParam(name = "session") final Session session, @WebParam(name = "task") final Task entity
    ) {
        sessionService.validate(session);
        return taskService.add(session.getUserId(), entity);
    }

    @WebMethod
    public Task addTaskWithName(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "name") String name,
            @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        return taskService.add(session.getUserId(), name, description);
    }

    @WebMethod
    public Task findTaskById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @WebMethod
    public void clearTask(@WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        taskService.removeById(session.getUserId(), id);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") final Session session, @WebParam(name = "task") final Task entity
    ) {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), entity);
    }

    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @WebMethod
    public Task findTaskByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.findByIndex(session.getUserId(), index);
    }

    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        taskService.removeByName(session.getUserId(), name);
    }

    @WebMethod
    public void removeTaskByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        taskService.removeByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "id") final String id,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        return taskService.updateById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "index") final Integer index,
            @WebParam(name = "name") final String name,
            @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        return taskService.updateByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    public Task startTaskById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @WebMethod
    public Task startTaskByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task startTaskByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @WebMethod
    public Task finishTaskById(
            @WebParam(name = "session") final Session session, @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.finishById(session.getUserId(), id);
    }

    @WebMethod
    public Task finishTaskByIndex(
            @WebParam(name = "session") final Session session, @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.finishByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task finishTaskByName(
            @WebParam(name = "session") final Session session, @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.finishByName(session.getUserId(), name);
    }

    @WebMethod
    public List<Task> findTaskByProjectId(
            @WebParam(name = "session") final Session session, @WebParam(name = "projectId") final String projectId
    ) {
        sessionService.validate(session);
        return projectTaskService.findTaskByProjectId(session.getUserId(), projectId);
    }

    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId,
            @WebParam(name = "projectId") final String projectId
    ) {
        sessionService.validate(session);
        projectTaskService.bindTaskById(session.getUserId(), taskId, projectId);
    }

    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session") final Session session, @WebParam(name = "taskId") final String taskId
    ) {
        sessionService.validate(session);
        projectTaskService.unbindTaskById(session.getUserId(), taskId);
    }

}
