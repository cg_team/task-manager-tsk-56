package ru.inshakov.tm.bootstrap;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.api.service.IReceiverService;
import ru.inshakov.tm.listener.LogMessageListener;
import ru.inshakov.tm.service.ReceiverService;

import static ru.inshakov.tm.constant.ActiveMQConst.URL;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    ReceiverService receiverService;

    public void start() {
        receiverService.receive(new LogMessageListener());
    }

}
